Add 
`````html`
    <link href="/style.22c40.css" rel="preload" as="style" onload="this.rel='stylesheet'">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="preload" as="style" onload="this.rel='stylesheet'">
    `````
    
To the build/index.html

Add this to:

````
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import { h } from "preact";
import MaterialComponent from "../MaterialComponent";

/**
 * @prop disabled = false
 */
export default class Icon extends MaterialComponent {
  constructor() {
    super();
    this.componentName = "icon";
  }
  materialDom(props) {
    return h(
      "i",
      _extends({}, props, { className: "material-icons" + " " + props.className}),
      props.children
    );
  }
}
````

node_modules/preact-material-components/Icon/index.js
Thank to it we will be able to add className to the icons and use syled-components with it ;)



node_modules/preact-cli/.../resources/template.html -> add google analytics into head
```
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107715651-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-107715651-1');
</script>
```
