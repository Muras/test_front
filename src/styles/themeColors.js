const color = {
  main: "#ff7a00",
  actionBlue: "#3b5998",
  desktopGrey: "#f7f7f7",
  mobileGrey: "#e5e5e5",
  fontGrey: "rgb(64, 64, 64)",
  brightGrey: "rgb(99,99,99)",
  black: "#000000",
  accent: "",
  alert: "",
  secondary: "",
  error: "red",
  success: "#659800",
  white: "#ffffff"
};

export default color;
