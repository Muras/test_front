import { h, Component } from "preact";
import Snackbar from "preact-material-components/Snackbar";
import "preact-material-components/Snackbar/style.css";
import Icon from "preact-material-components/Icon";

export default class SnackbarPage extends Component {
  componentDidMount() {
    this.callSnackbar();
  }

  callSnackbar = () => {
    this.bar.MDComponent.show({
      message: "<div><Icon>menu</Icon></div>Hello Snack!"
    });
  };

  render() {
    return (
      <div>
        <Button
          raised={true}
          primary={true}
          onClick={() => {
            this.bar.MDComponent.show({
              message: "<div><Icon>menu</Icon></div>Hello Snack!"
            });
          }}
        >
          Show snack
        </Button>
        <Snackbar
          dismissesOnAction
          ref={bar => {
            this.bar = bar;
          }}
        />
      </div>
    );
  }
}
