import styled from "styled-components";
import logo from "../assets/images/logo-small.png";

const Logo = styled.div`
  background: #ffffff;
  background-image: url(${logo});
  background-repeat: no-repeat;
  background-size: 100% 100%;
  width: 175px;
  height: 40px;
  display: inline-block;
`;

export default Logo;
