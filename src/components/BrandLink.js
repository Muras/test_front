import { h } from "preact";
import { Link } from "preact-router/match";
import Logo from "./Logo";
import styled from "styled-components";

const StyledLink = styled(Link)`
  text-decoration: none;
  font-size: 1.75em;
`;

const StyledLogo = styled(Logo)`
  margin-top: 0.25em;
`;

const BrandLink = ({ title }) => (
  <StyledLink href="/">
    <StyledLogo />
  </StyledLink>
);

export default BrandLink;
