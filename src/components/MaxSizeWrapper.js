import styled from "styled-components";
import { sizes } from "../styles/media";

const MaxSizeWrapper = styled.div`
  max-width: ${sizes.desktop + "px"};
  margin: 0 auto;
`;

export default MaxSizeWrapper;
