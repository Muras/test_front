import { h, Component } from "preact";
import styled from "styled-components";
import Fab from "preact-material-components/Fab";
import Menu from "preact-material-components/Menu";
import "preact-material-components/Fab/style.css";
import Icon from "preact-material-components/Icon";
import color from "../../styles/themeColors";
import SocialButtonsList from "../../containers/SocialButtonsList";


const ShareIcon = styled(Icon)`
  padding: 5px;
  font-size: 18px !important;
`;

const StyledFab = styled(Fab)`
  background: ${color.actionBlue} !important;
  width: 27px !important;
  height: 27px !important;
  float: right;
`;

export default class extends Component {
  constructor() {
    super();

    this.menu = null;
  }

  setMenuNode = node => {
    this.menu = node;
  };

  render({ className, ...otherProps }) {
    return (
      <Menu.Anchor className={className}>
        <StyledFab
          mini={true}
          ripple={true}
          onClick={() => {
            this.menu._component.MDComponent.open = true;
          }}
        >
          <ShareIcon>share</ShareIcon>
        </StyledFab>
        <SocialButtonsList {...otherProps} setMenuNode={this.setMenuNode} />
      </Menu.Anchor>
    );
  }
}
