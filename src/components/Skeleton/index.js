import { h } from "preact";
import SkeletonBody from "./SkeletonBody";
import SkeletonWrapper from "./SkeletonWrapper";

const Skeleton = ({ big }) => (
  <SkeletonWrapper>
    <SkeletonBody />
    {big ? <SkeletonBody /> : ""}
  </SkeletonWrapper>
);

export default Skeleton;
