import styled from "styled-components";
import Wrapper from "../Wrapper";

const SkeletonWrapper = styled(Wrapper)`
  background: white;
  margin-top: 1.5em;
  padding: 1em !important;
`;

export default SkeletonWrapper;
