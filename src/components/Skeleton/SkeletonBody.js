import styled from "styled-components";
import color from "../../styles/themeColors";

const SkeletonBody = styled.div`
  &:empty {
    background-color: ${color.white};
    margin: auto;
    width: 100%;
    height: 400px; /* change height to see repeat-y behavior */

    background-image: radial-gradient(
        circle 30px at 30px 30px,
        ${color.desktopGrey} 99%,
        transparent 0
      ),
      linear-gradient(
        100deg,
        rgba(255, 255, 255, 0),
        rgba(255, 255, 255, 0.5) 50%,
        rgba(255, 255, 255, 0) 80%
      ),
      linear-gradient(${color.mobileGrey} 15px, transparent 0),
      linear-gradient(${color.mobileGrey} 15px, transparent 0),
      linear-gradient(${color.mobileGrey} 15px, transparent 0),
      linear-gradient(${color.mobileGrey} 15px, transparent 0);

    background-repeat: repeat-y;

    background-size: 100px 80px, 50px 200px, 300px 200px, 350px 200px,
      300px 200px, 250px 200px;

    background-position: 0 0, 0 0, 120px 0, 120px 40px, 120px 80px, 120px 120px;

    animation: shine 1s infinite;
  }

  @keyframes shine {
    to {
      background-position: 0 0, 100% 0, 120px 0, 120px 40px, 120px 80px,
        120px 120px;
    }
  }
`;

export default SkeletonBody;
