import Card from "preact-material-components/Card";
import styled from "styled-components";
import { media } from "../styles/media";
import "preact-material-components/Card/style.css";

const Wrapper = styled(Card)`
  border: 1px solid #fff;
  background-color: #fff;
  margin: 10px 8px;
  padding-bottom: 10px !important;
  ${media.tablet`
    box-shadow: none;
  `};
`;

export default Wrapper;
