import styled from "styled-components";

const TextArea = styled.div`
  width: 100%;
  background: white;
  margin-top: 2em;
  ${props =>
    props.center ? "text-align: center;" : ""} padding: 2em 1em 3em 1em;
  position: relative;
`;

export default TextArea;
