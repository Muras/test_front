import styled from "styled-components";

const Wrapper = styled.div`
  ${props =>
    props.margin ? "margin: 0.5em 0 0.5em 0 !important;" : ""} font-size: 0.7em;
  display: flex;
`;

export default Wrapper;
