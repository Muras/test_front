import styled from "styled-components";
import Thumb from "./Thumb";
import Wrapper from "./Wrapper";

const Horizontal = styled.div`
  width: 50%;
  text-align: center;
  display: flex;
  flex-direction: column;
`;

const Count = styled.span`
  margin: 0 auto;
`;

const Votes = ({ votes, margin }) => (
  <Wrapper margin={margin}>
    <Horizontal>
      <Thumb positive>thumb_up</Thumb>
      <Count>{votes.positive_votes}</Count>
    </Horizontal>
    <Horizontal>
      <Thumb negative>thumb_down</Thumb>
      <Count>{votes.negative_votes}</Count>
    </Horizontal>
  </Wrapper>
);

export default Votes;
