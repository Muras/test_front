import styled from "styled-components";
import Icon from "preact-material-components/Icon";
import color from "../../styles/themeColors";

const Thumb = styled(Icon)`
  padding: 5px;
  ${props => (props.positive ? `color: ${color.success}` : "")};
  ${props => (props.negative ? `color: ${color.error}` : "")};
`;

export default Thumb;
