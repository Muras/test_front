import { h } from "preact";
import styled from "styled-components";
import { sizes, media } from "../styles/media";
import color from "../styles/themeColors";
import Icon from "preact-material-components/Icon";

import Card from "preact-material-components/Card";

const Text = styled(Card.Title)`
  text-align: center;
  transition-duration: 0.1s;
  font-size: 1.2em !important;
`;

const SubIcon = styled(Card.Subtitle)`
  text-align: center;
  padding-top: 7px;
`;

const StyledIcon = styled(Icon)`
  ${props => (props.toggleIcon ? "transform: rotate(180deg);" : "")};
`;

const Wrapper = styled(Card)`
  background: ${color.white};
  margin: auto;
  padding: 10px !important;
  cursor: pointer;
  user-select: none;
  max-width: ${props => (props.phone ? `max-width: ${sizes.phone}px;` : "")};

  ${media.tablet`
    box-shadow: none;
  `};

  &:hover {
    background: rgba(0, 0, 0, 0.1);
  }

  &:active {
    background: rgba(0, 0, 0, 0.2);
  }
`;

const ActionButton = ({
  handleOnClick,
  icon,
  label,
  className,
  toggleIcon
}) => (
  <Wrapper className={className} onClick={handleOnClick}>
    <Text>{label}</Text>
    <SubIcon>
      <StyledIcon toggleIcon={toggleIcon}>{icon}</StyledIcon>
    </SubIcon>
  </Wrapper>
);

export default ActionButton;
