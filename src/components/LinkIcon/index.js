import { h, Component } from "preact";
import styled from "styled-components";
import "preact-material-components/Fab/style.css";
import Icon from "preact-material-components/Icon";
import color from "../../styles/themeColors";

const StyledIcon = styled(Icon)`
  padding: 5px;
  font-size: 18px !important;
  transform: rotate(45deg);
  border: 2px ${color.brightGrey} solid; 
  border-radius: 50%;
`;

const HiddenInput = styled.input`
	position: absolute;
	opacity: 0;
`;

export default class extends Component {
	constructor() {
		super();

		this.input = null;

		this.state = {
			copying: false,
		};
	}

	render = ({ link }, { copying }) => {
		return (
	<div>
		{copying && <HiddenInput
			ref={node => { this.input = node; }}
			value={link}
		/>}
		<StyledIcon
			onClick={() => {
				this.setState({ copying: true }, () => {
					this.input.base.select();
					document.execCommand('copy');
					this.setState({ copying: false });
				});
			}}
		>
			link
		</StyledIcon>
	</div>
	);
	}
}

