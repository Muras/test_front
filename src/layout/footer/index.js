import { h, Component } from "preact";
import { BottomNavigation } from "../navigation/index";

class Footer extends Component {
  render() {
    return (
      <footer>
        <BottomNavigation />
      </footer>
    );
  }
}

export default Footer;
