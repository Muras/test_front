import { Link } from "preact-router/match";
import styled from "styled-components";
import color from "../../../styles/themeColors";

const StyledLink = styled(Link)`
  display: inline-block;
  padding: 12px 15px;
  min-width: 50px;
  text-align: center;
  text-decoration: none;
  margin: 0 5px;
  color: ${color.main};
  border: ${props => (props.active ? color.main : color.white)} 2px solid;
  border-radius: 50000px;
  will-change: background-color;

  &.active {
    border-color: ${color.main};
  }
`;

export default StyledLink;
