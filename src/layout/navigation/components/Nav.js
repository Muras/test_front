import styled from "styled-components";
import { media } from "../../../styles/media";
import color from "../../../styles/themeColors";

const Nav = styled.div`
  width: 100%;
  position: fixed;

  ${props =>
    props.top ? `${media.tablet`position: absolute;`}` : ""} ${props =>
      props.top ? "top: 0;" : ""} ${props =>
      props.bottom ? "bottom: 0;" : ""} padding-top: 5px;
  transform-origin: top;
  transition-duration: 0.1s;
  transition-timing-function: ease-out;
  background: ${color.white};
`;

export default Nav;
