import { h, Component } from "preact";
import { route, getCurrentUrl } from "preact-router";
import styled from "styled-components";
import color from "../../styles/themeColors";
import { media } from "../../styles/media";
import Tabs from "preact-material-components/Tabs";
import "preact-material-components/Tabs/style.css";
import Icon from "preact-material-components/Icon";

import Nav from "./components/Nav";

// Problems with styled-componenets, had to resolve this way. Fast fix ;)
const styles = {
  tabs: {
    background: color.white,
    width: "100%"
  }
};

const BottomWrapper = styled(Nav)`
  display: none;

  ${media.tablet`
      display: block;
   `};
`;

class BottomNavigation extends Component {
  constructor() {
    super();

    this.elStyle = {};
    this.state = {
      activeTab: getCurrentUrl()
    };
  }

  componentDidMount() {
    this.elStyle = this.base.style;
  }

  handleOnClick = path => {
    this.switchRoutes(path);
    this.setState({
      activeTab: path
    });
  };

  switchRoutes(path) {
    route(path, true);
  }

  render(props, { activeTab }) {
    return (
      <BottomWrapper bottom width={this.elStyle.width}>
        <Tabs style={styles.tabs} indicator-accent={true}>
          <Tabs.Tab
            ref={tab => {
              this.commentTab = tab;
            }}
            aria-label="Comments"
            active={"/" === activeTab}
            onClick={() => {
              this.handleOnClick("/");
            }}
          >
            <Icon>comment</Icon>
          </Tabs.Tab>
          <Tabs.Tab
            active={"/about" === activeTab}
            ref={tab => {
              this.aboutTab = tab;
            }}
            onClick={() => {
              this.handleOnClick("/about");
            }}
          >
            <Icon>live_help</Icon>
          </Tabs.Tab>
        </Tabs>
      </BottomWrapper>
    );
  }
}

export default BottomNavigation;
