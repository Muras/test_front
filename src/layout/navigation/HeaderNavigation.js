import { h, Component } from "preact";
import { getCurrentUrl } from "preact-router";
import styled from "styled-components";
import BrandLink from "../../components/BrandLink";
import { media } from "../../styles/media";
import Nav from "./components/Nav";
import StyledLink from "./components/StyledLink";
import MaxSizeWrapper from "../../components/MaxSizeWrapper";
import { debounce } from "../../lib/utils";

const LinkWrapper = styled.div`
  float: right;
  ${media.tablet`
   display: none;  
`};
`;

const TopNav = styled(Nav)`
  padding-bottom: 5px;
  z-index: 10000;
`;

class HeaderNavigation extends Component {
  constructor() {
    super();

    this.state = {
      currentBackground: "white"
    };
    this.debouncedToggleVisibility = debounce(this.toggleVisibility, 250);
  }
  elStyle = {};
  scrollLimit = 20;
  previousScroll = 0;

  componentDidMount() {
    this.elStyle = this.base.style;
    window.addEventListener("scroll", this.debouncedToggleVisibility);
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.debouncedToggleVisibility);
  }

  toggleVisibility = () => {
    const scrollCondition = window.scrollY > this.scrollLimit;

    if (this.previousScroll < window.scrollY) {
      this.elStyle.transform = "scaleY(0)";
    } else {
      this.elStyle.transform = "scaleY(1)";
    }

    this.previousScroll = window.scrollY;
    if (scrollCondition) {
      this.elStyle.boxShadow =
        "0 1px 3px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.04)";
    } else {
      this.elStyle.boxShadow = "none";
    }
  };

  render() {
    return (
      <TopNav top style={{ background: this.state.currentBackground }}>
        <MaxSizeWrapper>
          <BrandLink />
          <LinkWrapper>
            <StyledLink activeClassName="active" href="/">
              Komentarze
            </StyledLink>
            <StyledLink activeClassName="active" href="/about">
              O stronie
            </StyledLink>
          </LinkWrapper>
        </MaxSizeWrapper>
      </TopNav>
    );
  }
}

export default HeaderNavigation;
