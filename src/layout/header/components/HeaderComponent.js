import styled from "styled-components";
import color from "../../../styles/themeColors";

const Wrapper = styled.header`
  background: ${color.white};
  color: #fff;
  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 0 0 1px rgba(0, 0, 0, 0.04);
`;

const Container = styled.div`
  margin: 0 auto;
  padding: 20px 0;
  height: 4em;
`;

const HeaderContainer = ({ title, description, children }) => (
  <Wrapper>
    <Container>{children}</Container>
  </Wrapper>
);

export default HeaderContainer;
