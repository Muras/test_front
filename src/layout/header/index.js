import { h, Component } from "preact";
import HeaderComponent from "./components/HeaderComponent";
import { HeaderNavigation } from "../navigation/index";

class Header extends Component {
  render() {
    return (
      <HeaderComponent>
        <HeaderNavigation />
      </HeaderComponent>
    );
  }
}

export default Header;
