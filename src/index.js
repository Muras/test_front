import App from "./containers/app";
import "./global-styles";

export default () => (
  <div class="app">
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
    />
    <App />
  </div>
);
