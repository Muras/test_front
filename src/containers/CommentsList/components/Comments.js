import { h } from "preact";
import LoadMoreButton from "./LoadMoreButton";
import styled from "styled-components";
import Skeleton from "../../../components/Skeleton";

const Wrapper = styled.div`
  margin-bottom: 5em;
`;

const Comments = ({ children, fetching, handleOnClick, hidden }) => {
  const renderIfHidden = hidden ? (
    ""
  ) : (
    <LoadMoreButton
      handleOnClick={handleOnClick}
      label="Wczytaj komentarze"
      icon="autorenew"
      phone
    />
  );

  const renderIfFetching = fetching ? <Skeleton /> : "";

  return (
    <Wrapper>
      {children}
      {renderIfFetching}
      {renderIfHidden}
    </Wrapper>
  );
};

export default Comments;
