import { h } from "preact";
import ActionButton from "../../../components/ActionButton";
import styled from "styled-components";
import { sizes } from "../../../styles/media";

const LoadMoreButton = styled(ActionButton)`
  max-width: ${sizes.phone}px;
`;

export default LoadMoreButton;
