import { h, Component } from "preact";
import { route } from 'preact-router';
import api from "../../lib/api";
import CommentsCard from "../CommentCard/CommentCard";
import Comments from "./components/Comments";
import Skeleton from "../../components/Skeleton";

class CommentsList extends Component {
  constructor() {
    super();

    this.loadLimit = 15;

    this.state = {
      loading: true,
      error: false,
      fetching: false,
      results: [],
      offset: 0,
      count: 0,
      limit: 4,
      windowWidth: window.innerWidth
    };
  }

  componentDidMount() {
    if (this.state.loading) {
      this.loadComments(this.state.limit, this.state.offset);
    }

    window.addEventListener("orientationchange", this.handleOrientationChange);
  }

  componentWillUnmount() {
    window.removeEventListener(
      "orientationchange",
      this.handleOrientationChange
    );
  }

  getCommentById(id) {
    api
      .getSingleComment(id)
      .then(res => {
        res.data.sharedComment = true;

        this.setState(prevState => ({
          results: [res.data, ...prevState.results]
        }));
      })
      .catch(exception => {
        this.setState({
          loading: false,
          fetching: false,
          error: exception,
        });
				route('/');
			});
  }

  handleOnClick = () => {
    this.loadComments(this.loadLimit, this.state.offset);
  };

  loadComments = (limit, offset) => {
    this.setState({ fetching: true });

    api
      .fetchData(limit, offset)
      .then(res => {
        const { results, offset, count, limit } = res.data;

        this.setState({
          offset: offset + res.data.results.length,
          count,
          limit,
          results: [...this.state.results, ...results],
          loading: false,
          fetching: false
        });
      })
      .catch(err => {
        this.setState({ loading: false, fetching: false });
      });

    if (this.props.commentId) {
      this.getCommentById(this.props.commentId);
    }
  };

  handleOrientationChange = () => {
    this.setState({ windowWidth: window.innerWidth });
  };

  render({}, { loading, fetching, results, windowWidth }) {
    if (loading) {
      return <Skeleton big />;
    }
    return (
      <Comments
        handleOnClick={this.handleOnClick}
        hidden={this.state.count === this.state.offset}
        fetching={fetching}
      >
        <div>
          {results.map(comment => (
            <CommentsCard windowWidth={windowWidth} {...comment} />
          ))}
        </div>
      </Comments>
    );
  }
}

export default CommentsList;
