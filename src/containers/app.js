import { h, Component } from "preact";
import { Router, route } from "preact-router";
import Header from "../layout/header";
import Footer from "../layout/footer";
import Comments from "../pages/comments";
import About from "../pages/about";

export default class App extends Component {
  /** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
  routes = ["/", "/about"];

  handleRouteChange = e => {
    route(this.urlVerification(e.url));
  };

  urlVerification(url) {
    if (this.routes.includes(url)) return url;
    else return "/";
  }

  render() {
    return (
      <div id="app">
        <Header />
        <Router onChange={this.handleRouteChange}>
          <About path="/about" />
          <Comments path="/:commentId" />
          <Comments default />
        </Router>
        <Footer />
      </div>
    );
  }
}
