import Card from "preact-material-components/Card";
import styled from "styled-components";
import "preact-material-components/Card/style.css";

const Title = styled(Card.Title)`
  display: inline-block;
  font-size: 120% !important;
  line-height: 110% !important;
  margin-bottom: 0.1em !important;
`;

export default Title;
