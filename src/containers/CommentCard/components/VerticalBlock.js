import styled from "styled-components";

const VerticalBlock = styled.div`
  display: flex;
  flex-direction: column;
`;

export default VerticalBlock;
