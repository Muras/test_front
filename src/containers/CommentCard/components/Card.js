import styled from "styled-components";
import ShareIcon from "../../../components/ShareIcon";
import Domain from "./Domain";
import Wrapper from "../../../components/Wrapper";
import Image from "./Image";
import Title from "./Title";
import Primary from "./Primary";
import Votes from "../../../components/Votes/index";
import ExpandButton from "./ExpandButton";
import Comment from "./Comment";
import HorizontalBlock from "./HorizontalBlock";
import VerticalBlock from "./VerticalBlock";
import TimeFromCreation from "./TimeFromCreation";
import A from "../../../components/A";
import { sizes } from "../../../styles/media";
import color from "../../../styles/themeColors";

const CardWrapper = styled(Wrapper)`
  position: relative;
  transition-duration: 0.2s;
  ${props => (props.isShare ? `border-color: ${color.error}` : "")};
`;

const StyledShareIcon = styled(ShareIcon)`
  position: absolute !important;
  right: 5px;
`;

const CardComponent = ({
  id,
  title,
  image,
  badges,
  comment,
  domain,
  negative_votes,
  positive_votes,
  url,
  timeFormat,
  expendable,
  expend,
  handleOnClick,
  windowWidth,
  sharedComment,

}) => (
  <CardWrapper isShare={sharedComment}>
    <HorizontalBlock>
      <VerticalBlock>
        <A href={url} target="_blank" rel="noopener">
          <Image src={image} />
        </A>
        {windowWidth <= sizes.phone ? (
          <Votes margin votes={{ negative_votes, positive_votes }} />
        ) : (
          ""
        )}
      </VerticalBlock>

      <Primary>
        <A href={url} target="_blank" rel="noopener">
          <Title large>{title}</Title>
        </A>
        <div>
          <Domain>{domain}</Domain> -{" "}
          <TimeFromCreation>{timeFormat}</TimeFromCreation>
        </div>
      </Primary>
      {windowWidth > sizes.phone ? (
        <Votes margin votes={{ negative_votes, positive_votes }} />
      ) : (
        ""
      )}
    </HorizontalBlock>
    <Comment expend={expend} expendable={expendable}>
      {'"'}
      {comment}
      {'"'}
    </Comment>
    {expendable ? (
      <ExpandButton
        handleOnClick={handleOnClick}
        icon="keyboard_arrow_down"
        toggleIcon={expend}
      />
    ) : (
      ""
    )}
		<StyledShareIcon commentId={id} comment={comment} />

  </CardWrapper>
);

export default CardComponent;
