import styled from "styled-components";
import { media } from "../../../styles/media";
import Card from "preact-material-components/Card";
import "preact-material-components/Card/style.css";

const Image = styled(Card.MediaItem)`
  max-width: 100px;
  max-height: 100px;
  margin: 0 !important;
  float: left;
  ${media.tablet`
    max-width: 73px;
    max-height: 73px;
  `};
`;

export default Image;
