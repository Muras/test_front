import color from "../../../styles/themeColors";
import styled from "styled-components";
import Card from "preact-material-components/Card";
import "preact-material-components/Card/style.css";

const Domain = styled(Card.Subtitle)`
  color: ${color.success} !important;
  display: inline-block;
`;

export default Domain;
