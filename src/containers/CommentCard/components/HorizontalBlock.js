import styled from "styled-components";
import Card from "preact-material-components/Card";
import "preact-material-components/Card/style.css";

const HorizontalBlock = styled(Card.HorizontalBlock)`
  padding: 10px 16px 0 16px !important;
`;

export default HorizontalBlock;
