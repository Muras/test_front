import styled from "styled-components";
import Card from "preact-material-components/Card";
import color from "../../../styles/themeColors";
import "preact-material-components/Card/style.css";

const Comment = styled(Card.SupportingText)`
  text-overflow: ellipsis;
  padding-top: 20px !important;
  padding-bottom: 10px;
  color: ${color.fontGrey} !important;

  & p {
    display: inline;
  }
  ${props =>
    props.expendable
      ? props.expend ? "" : "max-height: 6em;"
      : ""} margin-left: 16px;
  border-left: 5px solid ${color.mobileGrey};
  padding-bottom: 8px !important;
  overflow: hidden;
  transition-duration: 0.1s;
  transition-timing-function: ease-out;
`;

export default Comment;
