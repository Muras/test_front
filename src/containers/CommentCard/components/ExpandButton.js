import styled from "styled-components";
import ActionButton from "../../../components/ActionButton";

const whiteTopShadow = `
	  -webkit-box-shadow: 0px -16px 16px -3px rgba(255, 255, 255, 0.98) !important;
  -moz-box-shadow: 0px -16px 16px -3px rgba(255, 255, 255, 0.98) !important;
  box-shadow: 0px -16px 16px -3px rgba(255, 255, 255, 0.98) !important;
`;

const ExpandButton = styled(ActionButton)`
  ${props =>
    props.toggleIcon
      ? "box-shadow: none !important; "
      : whiteTopShadow} width: 100%;
  padding: 4px !important;
`;
export default ExpandButton;
