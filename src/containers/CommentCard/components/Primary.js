import styled from "styled-components";
import Card from "preact-material-components/Card";

const Primary = styled(Card.SupportingText)`
  width: 100%;
  padding-bottom: 0 !important;
`;

export default Primary;
