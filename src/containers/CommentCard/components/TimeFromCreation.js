import styled from "styled-components";
import color from "../../../styles/themeColors";

const TimeFromCreation = styled.span`
  color: ${color.fontGrey};
  display: inline-block;
`;

export default TimeFromCreation;
