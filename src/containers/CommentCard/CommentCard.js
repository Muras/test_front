import { h, Component } from "preact";
import Card from "./components/Card";

class CommentCard extends Component {
  constructor({ created_at }) {
    super({ created_at });
    this.state = {
      timeFormat: "",
      createdAt: new Date(created_at),
      expendable: false,
      commentHeight: 0,
      expend: false
    };
  }

  timeFromCreation = 0;
  commentHeight = 0;

  componentDidMount() {
    this.commentHeight = this.base.children[1].clientHeight;

    this.isExpendable(this.commentHeight);
    this.calculateTime();
    this.displayTime();
  }

  calculateTime() {
    this.timeFromCreation = (new Date() - this.state.createdAt) / (1000 * 3600);
  }

  displayTime = () => {
    if (!this.timeFromCreation) {
      this.setState({ timeFormat: "New!" });
    } else {
      this.setState({
        timeFormat: `${Math.round(this.timeFromCreation)} godzin temu`
      });
    }
  };

  toggleExpending = () => {
    this.setState({ expend: !this.state.expend });
  };

  isExpendable = commentHeight => {
    const maxCommentHeight = 140;
    maxCommentHeight < commentHeight && this.setState({ expendable: true });
  };

  render(props, state) {
    return (
        <Card
          {...props}
          {...state}
          handleOnClick={this.toggleExpending}
        />
    );
  }
}

export default CommentCard;
