import { h, Component } from "preact";
import styled from "styled-components";
import { ShareButtons, generateShareIcon } from "react-share";
import Menu from "preact-material-components/Menu";
import "preact-material-components/Menu/style.css";
import "preact-material-components/Button/style.css";
import LinkIcon from '../../components/LinkIcon';

const iconSize = 30;
const shareUrl = id => `https://komcie.pl/${id}`;

const {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  RedditShareButton
} = ShareButtons;

const FacebookIcon = generateShareIcon("facebook");
const TwitterIcon = generateShareIcon("twitter");
const WhatsappIcon = generateShareIcon("whatsapp");
const LinkedinIcon = generateShareIcon("linkedin");
const RedditIcon = generateShareIcon("reddit");

const StyledMenu = styled(Menu)`
  bottom: 0 !important;
  overflow: hidden !important;
  left: inherit !important;
  top: inherit !important;
  right: 0 !important;
  
  & ul {
    display: flex;
    margin: 0 !important;
    overflow: hidden;
    list-style-type: none;

    & li {
      margin: 0 5px;
      padding: 0 !important;
      
      & div {
        outline: none;
      }
    }
  }
`;

export default class SocialButtonsList extends Component {
  componentDidMount() {
    this.props.setMenuNode(this.menu);
  }

  render({ commentId, comment }) {
    return (
      <StyledMenu
        ref={node => {
          this.menu = node;
        }}
      >
        <Menu.Item>
          <WhatsappShareButton
            url={shareUrl(commentId)}
            title={comment}
            separator=":: "
          >
            <WhatsappIcon size={iconSize} round />
          </WhatsappShareButton>
        </Menu.Item>

        <Menu.Item>
          <LinkedinShareButton
            url={shareUrl(commentId)}
            title={comment}
            windowWidth={750}
            windowHeight={600}
          >
            <LinkedinIcon size={iconSize} round />
          </LinkedinShareButton>
        </Menu.Item>

        <Menu.Item>
          <RedditShareButton
            url={shareUrl(commentId)}
            title={comment}
            windowWidth={660}
            windowHeight={460}
            className="Demo__some-network__share-button"
          >
            <RedditIcon size={iconSize} round />
          </RedditShareButton>
        </Menu.Item>

        <Menu.Item>
          <TwitterShareButton
            url={shareUrl(commentId)}
            title={comment}
            className="Demo__some-network__share-button"
          >
            <TwitterIcon size={iconSize} round />
          </TwitterShareButton>
        </Menu.Item>

        <Menu.Item>
          <FacebookShareButton url={shareUrl(commentId)} quote={comment}>
            <FacebookIcon size={iconSize} round />
          </FacebookShareButton>
        </Menu.Item>

        <Menu.Item>
					<LinkIcon link={shareUrl(commentId)} />
        </Menu.Item>
      </StyledMenu>
    );
  }
}
