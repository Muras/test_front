import { Component, h, cloneElement } from 'preact';

export default (WrappedComponent) => {
	return class SwipeRecognizer extends Component {
		constructor() {
			super();

			this.isMousePressed = false;
			this.wrapped = null;
			this.gesture = { x: [], y: [], match: '', clientXStart: 0 };
			this.state = {
				swipe: '',
			}
		}

		componentDidMount() {
			this.base.addEventListener('touchstart', this.capture);
			this.base.addEventListener('touchmove', this.capture);
			this.base.addEventListener('touchend', this.compute);

			this.base.addEventListener('mousedown', this.capture);
			this.base.addEventListener('mousemove', this.capture);
			this.base.addEventListener('mouseup', this.compute);
			this.base.addEventListener('mouseout', this.compute);
		}

		componentWillUnmount() {
			this.base.removeEventListener('touchstart', this.capture);
			this.base.removeEventListener('touchmove', this.capture);
			this.base.removeEventListener('touchend', this.compute);

			this.base.removeEventListener('mousedown', this.capture);
			this.base.removeEventListener('mousemove', this.capture);
			this.base.removeEventListener('mouseup', this.compute);
            this.base.removeEventListener('mouseout', this.compute);

        }

		capture = (event) => {
			event.preventDefault();
			const { clientX, clientY } = event.touches || event;
			const eventType = event.type;

			if (eventType === 'mousedown') {
				this.gesture.x = [];
				this.gesture.y = [];
				this.gesture.match = '';
				this.gesture.clientXStart =  clientX;
				this.isMousePressed = true;
			}
            const pixelMove = clientX - this.gesture.clientXStart;

			if(this.isMousePressed) {
				this.wrapped.base.style.transform = `translateX(${pixelMove}px)`;
			}

			this.gesture.x.push(clientX);
			this.gesture.y.push(clientY);
		};

		compute = (event) => {
			const tolerance = 100;
			const xStart = this.gesture.x.shift();
			const yStart = this.gesture.y.shift();
			const xEnd = this.gesture.x.pop();
			const yEnd = this.gesture.y.pop();
			const xTravel = xEnd - xStart;
			const yTravel = yEnd - yStart;

			const isVertical = Math.abs(yTravel) > tolerance;
			const isHorizontal = Math.abs(xTravel) > tolerance;
			const isLeft = xTravel < 0;
			const isUp = yTravel < 0;

			if (isVertical && !isHorizontal) {
				isUp ? this.gesture.match = 'up' : this.gesture.match = 'down';
			}

			if (isHorizontal && !isVertical) {
				isLeft ? this.gesture.match = 'left' : this.gesture.match = 'right';
			}

			if (this.gesture.match !== ''){
				this.onSwipe(this.gesture.match);
			} else {
            }

			this.gesture.x = [];
			this.gesture.y = [];
			this.gesture.match = '';
			this.gesture.clientXStart = 0;
			this.isMousePressed = false;
            this.wrapped.base.style.transform = `translateX(0px)`;

        };

		onSwipe = (direction) => {

			if (this.props.onSwipe) {
				this.props.onSwipe(direction);
			}
			this.setState({ swipe: direction });
		};

		render(props, state) {
			return <WrappedComponent ref={(node) => { this.wrapped = node}} {...props} {...state} />
		}
	}
}