import axios from "axios";

const apiURL = `https://api.komcie.pl`;

axios.defaults.baseURL = apiURL;

class Api {
  fetchData = (limit, offset) => axios.get(`/${limit}/${offset}`);

  getSingleComment = id => axios.get(`comment/${id}`);
}

export default new Api();
