import { h, Component } from "preact";

import CommentsList from "../../containers/CommentsList";
import MaxSizeWrapper from "../../components/MaxSizeWrapper";

export default class Comments extends Component {
  render({ commentId }) {
    return (
      <MaxSizeWrapper>
        <CommentsList commentId={commentId} />
      </MaxSizeWrapper>
    );
  }
}
