import { h, Component } from 'preact';
import MaxSizeWrapper from '../../components/MaxSizeWrapper';
import TextArea from '../../components/TextArea';
import Wrapper from '../../components/Wrapper';
import Icon from 'preact-material-components/Icon';
import styled from 'styled-components';

const Authors = styled.div`
  position: absolute;
  right: 1em;
  bottom: 0.5em;
`;

const PageWrapper = styled(Wrapper)`
  margin-bottom: 30px;
`;

export default class TabsPage extends Component {
	render() {
		return (
			<MaxSizeWrapper>
				<PageWrapper>
					<TextArea center>
						<p>
              Aplikacja powstała wyłącznie dla celów doświadczalnych i
              naukowych.
						</p>
						<Icon>bug_report</Icon>
						<p>
              Zastosowanie i pożytek praktyczny jest wielce nieznany,
              szczególnie autorom danego dzieła.
						</p>
						<Icon>bug_report</Icon>
						<p>
              Mamy nadzieję, iż umili niektórym życie i pozwoli zaoszczędzić
              czas, który tracili na poszukiwaniu najlepszych komentarzy.
						</p>
						<Icon>bug_report</Icon>
						<p>
              Jeśli komukolwiek pomysł i inicjatywa się podoba, zapraszam do
              kontaktu...
							<br />
							<strong>TODO: </strong> formularz kontaktowy :)
						</p>
						<Authors>
							<i>by Mickey and Skopke</i>
						</Authors>
					</TextArea>
				</PageWrapper>
			</MaxSizeWrapper>
		);
	}
}
