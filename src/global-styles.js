import { injectGlobal } from "styled-components";
import color from "./styles/themeColors";
import { media } from "./styles/media";

/**
 * Includes body and NAV / ahref styles
 */

injectGlobal`
	html, body {
		height: 100%;
		width: 100%;
		padding: 0;
		margin: 0;
      background: ${color.desktopGrey};
		font-family: 'Helvetica Neue', arial, sans-serif;
		font-weight: 400;
		color: #444;
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}

   ${media.tablet`
      html, body {
         background: ${color.mobileGrey}
      }
   `}
   
	* {
		box-sizing: border-box;
	}

	#app {
		height: 100%;
	}

	body {
		margin: 0 auto;
		overflow: scroll !important;
	}

	nav {
		float: right;
		font-size: 100%;
	}
	
	
`;
