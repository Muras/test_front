/**
 * Function that mutates original webpack config.
 * Supports asynchronous changes when promise is returned.
 *
 * @param {object} config - original webpack config.
 * @param {object} env - options passed to CLI.
 * @param {WebpackConfigHelpers} helpers - object with useful helpers when working with config.
 **/
const preactCliSwPrecachePlugin = require('preact-cli-sw-precache');
// const HtmlCriticalPlugin = require('html-critical-webpack-plugin');
// const path = require('path');

export default function (config, env, helpers) {
    const precacheConfig = {
      staticFileGlobs: [
        'build/*.css',
        'build/*.js',
        'build/*.html',
        'build/assets/images/logo-small.png',
        'build/assets/badges/*.svg',
        'build/assets/icons/*.png',
      ],
      stripPrefix: 'build/',
      minify: true,
      navigateFallback: 'index.html',
      runtimeCaching: [{
        urlPattern: /.(wav|png)/,
        handler: 'cacheFirst'
      }],
      filename: 'sw.js',
      clientsClaim: true,
      skipWaiting: true,
      dontCacheBustUrlsMatching: /^(?=.*\.\w{1,7}$)/,
      staticFileGlobsIgnorePatterns: [
        /polyfills(\..*)?\.js$/,
        /\.map$/,
        /push-manifest\.json$/,
        /.DS_Store/
      ]
    };   

    return preactCliSwPrecachePlugin(config, precacheConfig);
  }